/*

computes dynamic external ray for complex quadratic polynomial using Newton method

c console program
----------------------------
Adam Majewski
fraktal.republika.pl
https://commons.wikimedia.org/w/index.php?title=Special:ListFiles/Adam_majewski&ilshowall=1

it contains code by other people see description of the functions

license GPL3+ <http://www.gnu.org/licenses/gpl.html>


---------- how to use ? -----------------------------------------------
to compile with gcc :
 gcc n.c -std=c99 -Wall -pedantic -Wextra -O3 -lgmp -lm

to run from console ( prints output to console )): 
./a.out

or ( to print outout to ray.txt file ): 

./a.out > ray.txt




------------------- example output ------------------------------------------- 


0.244374793756547387 0.097414596966656580
0.244376540955804800 0.097415295902512994
0.244377945791660001 0.097415784193484747
0.244379102137667281 0.097416142850940371
0.244380077129334072 0.097416419260454218
 internal angle = 1 /  30 
  external angle = 1 /  1073741823 
  c = 0.2606874359562527 ; 0.0022716846399296 
  z_alfa = 0.4890738003669028 ; 0.1039558454088799 
  z_last = 0.2443800771293341 ; 0.0974164192604542 
  distance between the last point of the ray and the "true" landing point is 
 = 0.2447810905405371 in world units = 82 pixels = 8 percent 


--------------- algorithm ------------------------------------------
-   choose period of Parent hyperbolic Component of Mandelbrot set ( iPeriodParent)
 ( now only works for 1 and 2 )
- choose internal angle ( or rotational number ) 
- compute root point between parent and child hyperbolic components ( period of the child component = 
  denomiunator of internal angle )
- compute angle of external parameter ray that lands on the above root point :
  numerator is the same as in internal angle
  denominator : ea_denominator = 2^ia_denominator - 1 
 ( on dynamic plane the same ray lands on the fixed point )
- compute parameter c and fixed point alfa ( z_alfa )
- compute external dynamic ray for angle = ea_numerator /  ea_denominator
- find distance between last computed point of the dynamic ray ( z_last )
  and parabolic fixed point alfa ( z_alfa), which should be it's landing point


------ how to plot using gnuplot --------------------------
gnuplot
gnuplot> set yrange [-2:2]   
gnuplot> set xrange [-2:2]
         set title "dynamic external ray"
         set lt 1 lc 3  pt 7 ps 1.5
        plot "ray.txt" with lines
        plot "ray.txt" with linespoints






------------ double precision exhausted ----------------

1203.777144430312091572 1917.724142051356011507
25.294948749738445315 40.295149865579780624
3.646140536381642150 5.796012068765143077
1.333999068520909326 2.106125856808257701
0.737554160745237497 1.191104290957139655
0.481736764269270479 0.855366429927661875
0.334361738231624850 0.706690440955912602
0.236473541392382802 0.636912885522791794
0.170348634344165889 0.600438076693124878
... ( removed lines ) .......
-0.034169965514066356 0.459572536242767338
-0.034169965595214354 0.459572536362930772
-0.034169965637848299 0.459572536397342857
-0.034169965657350566 0.459572536404943388
-0.034169965665898576 0.459572536405666254
double precision exhausted




*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // strcat
#include <complex.h>
#include <math.h>
#include <gmp.h>


/* --------------------- global variables --------------- */
 const double pi = 3.14159265358979323846264338327950288419716939937510;








/* ------------------ functions --------------------------*/


/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html

*/
double complex GiveC(unsigned long long int numerator, unsigned long long int denominator, double InternalRadius, unsigned long long int ParentPeriod)
{
  
  
  double t = 2.0*pi*numerator/denominator; // Internal Angle from turns to radians 
  double R2 = InternalRadius * InternalRadius;
  double Cx, Cy; /* C = Cx+Cy*i */

  switch ( ParentPeriod ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each ParentPeriod  there are 2^(ParentPeriod-1) roots. 
    default: // higher periods : not works, to do
      Cx = 0.0;
      Cy = 0.0; 
      break; }

  return Cx + Cy*I;
}


/*
 
  http://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings
  z^2 + c = z
  z^2 - z + c = 0
  ax^2 +bx + c =0 // ge3neral for  of quadratic equation
  so :
  a=1
  b =-1
  c = c
  so :
 
  The discriminant is the  d=b^2- 4ac 
 
  d=1-4c = dx+dy*i
  r(d)=sqrt(dx^2 + dy^2)
  sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx +- sy*i
 
  x1=(1+sqrt(d))/2 = beta = (1+sx+sy*i)/2
 
  x2=(1-sqrt(d))/2 = alfa = (1-sx -sy*i)/2
 
  alfa : attracting when c is in main cardioid of Mandelbrot set, then it is in interior of Filled-in Julia set, 
  it means belongs to Fatou set ( strictly to basin of attraction of finite fixed point )
 
*/
// uses global variables : 
//  ax, ay (output = alfa(c)) 
double complex GiveAlfaFixedPoint(double complex c)
{
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  double sx, sy; // s = sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx + sy*i
  double ax, ay;
 
  // d=1-4c = dx+dy*i
  dx = 1 - 4*creal(c);
  dy = -4 * cimag(c);
  // r(d)=sqrt(dx^2 + dy^2)
  r = sqrt(dx*dx + dy*dy);
  //sqrt(d) = s =sx +sy*i
  sx = sqrt((r+dx)/2);
  sy = sqrt((r-dx)/2);
  // alfa = ax +ay*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
  ax = 0.5 - sx/2.0;
  ay =  sy/2.0;
 
 
  return ax+ay*I;
}
 



/*

Function computes dynamic external ray
for fc(z)=z^2+c

GiveDynamicRay(1,3,4, 25)

code by Claude Heiland-Angle
http://mathr.co.uk/blog/
https://gitorious.org/~claude

License GPL3+ <http://www.gnu.org/licenses/gpl.html>
*/
double complex GiveDynamicRay(double complex c, unsigned long long int numerator, unsigned long long int denominator, unsigned long long int sharpness, unsigned long long int depth  )
{
  mpq_t angle;
  mpq_init(angle);
  mpq_set_ui(angle, numerator, denominator);
  mpq_canonicalize(angle);
   
  
  // one = 1/1
  mpq_t one;
  mpq_init(one);
  mpq_set_si(one, 1, 1);
  // escape radius
  const double R = 55536;
  // limit count for Newton's method
  const int newtonSteps = 64;
  // threshold for Newton's method convergence
  const double epsilon = 1e-12;
  

  // initial point on circle radius R
  complex double oldZ = R * cexp(2 * pi * I * mpq_get_d(angle));
  

  // step through dwell bands
  for (unsigned long long int  dwell    = 0; dwell <= depth; ++dwell) {
  
     // step within dwell band
    for (unsigned long long int step = 1; step <= sharpness; ++step) {
   
      // target radius
      double radius = pow(R, pow(0.5, (step - 0.5) / sharpness));
      // target point
      complex double target = radius * cexp(2 * pi * I * mpq_get_d(angle));
      // starting guess for Newton's method
      complex double zPrev = oldZ;
   

      // find c such that f_c^dwell(0) = t where f_c(z) = z^2 + c
      for (int newtonStep = 0; newtonStep < newtonSteps; ++newtonStep) {
        // Newton's method step
        complex double z = zPrev; // 
        complex double dz = 1.0; // d/dz for dynamic plane 
        for (unsigned long long int i = 0; i < dwell; ++i) {
          dz = 2 * z * dz ;
          z = z * z + c;
        }
   
       complex double zNext = zPrev - (z - target) / dz;
  
        // check for convergence or NaN
        if (! (cabs(zNext - zPrev) > epsilon)) {
          break;
        }
        zPrev = zNext;
      }
  
      // use zPrev instead of zNext in case of NaN
      complex double newZ = zPrev;
  
      // check if ray points are distinct
      if (newZ == oldZ) {
        fprintf(stderr, "double precision exhausted\n");
        goto done;
      }
  
     // output ray point
      printf("%.18f %.18f\n", creal(newZ), cimag(newZ));
      oldZ = newZ;
    }
 
     // double angle (mod 1) when incrementing dwell      
    //mpq_mul_2exp(angle, angle, 1);
     mpq_add(angle, angle, angle); // angle = angle+angle 
    
    if (mpq_cmp_ui(angle, 1, 1) >= 0) {
      mpq_sub(angle, angle, one); // modulo 1
    }
  }

 done:
 //  gmp_printf ("last angle = %Qd\n",angle ); // check angle 
  // cleanup
  mpq_clear(one);
  mpq_clear(angle);


return oldZ; // last point of the ray 

}



// https://en.wikipedia.org/wiki/Euclidean_distance
double GiveDistance( complex double z1, complex double z2)
{
  double dx,dy;

  dx = creal(z1)-creal(z2);
  dy = cimag(z1)-cimag(z2);
  
  return (sqrt(dx*dx +dy*dy ));

}



// save info txt file named ea_numerator.txt where ea_numerator is a 
int SaveInfoTextFile( unsigned long long int iPeriodParent,  
			unsigned long long int ia_numerator, unsigned long long int ia_denominator, 
                      unsigned long long int ea_numerator, unsigned long long int ea_denominator,
                      //unsigned long long int iPeriodChild, 
                      complex double c, complex double z_alfa, complex double z_last,
                      double PixelWidth, double distance)
{
 
  FILE * fp;
  char name [100]; /* name of file  without extension  */
  char *filename;
  snprintf(name, sizeof name, "%llu", ea_numerator); /*  */
 
 
 
  // save info text filename
 filename =strcat(name,".txt");
 fp= fopen(filename,"wb");
 
 fprintf(fp,"This is dynamic plane of discrete complex dynamical system  z(n+1) = fc(zn) \n");
 fprintf(fp," where  fc(z)= z^2 + c \n");
 fprintf(fp,"with numerical approximation of dynamic external ray  \n\n");
 fprintf(fp,"parameter c is a root point between iPeriodParent = %llu and iPeriodOfChild  = %llu hyperbolic components of Mandelbrot set \n", iPeriodParent , ia_denominator);
 fprintf(fp,"it is the landing point of the rays : \n ");
 fprintf(fp,"- the internal ray of parent component of Mandelbrot set with angle = %llu/%llu in turns \n", ia_numerator, ia_denominator);
 fprintf(fp,"- the external ray of with angle = %llu/%llu in turns \n",ea_numerator, ea_denominator);
 fprintf(fp,"aproximated value of c = ( %.16f ; %.16f ) \n", creal(c), cimag(c)); 
 fprintf(fp," \n");
 fprintf(fp,"parabolic alfa fixed point z_alfa = ( %f ; %f )  \n", creal(z_alfa), cimag(z_alfa)); 
 fprintf(fp,"Pixel width = %.16f   \n", PixelWidth);
 fprintf(fp,"Last computed point of the extrnal ray is z_last = %.16f ; %.16f \n ", creal(z_last), cimag(z_last));
 fprintf(fp,"distance between the last point of the ray and the \"true\" landing point is :\n");
 fprintf(fp," = %.16f in world units = %.0f pixels = %.0f percent \n", distance, distance/PixelWidth , 100*distance/(PixelWidth*1000));  //
 fprintf(fp,"when image height = 1000 pixels and 3.0 world units \n"); 
 fprintf(fp,"\nmade with c console program  \n");
 
 printf("File %s saved. \n", filename);
  
 fclose(fp); 
 
 
  return 0;
}



unsigned long long int iPower(unsigned long long int base, unsigned long long int exp)
{
    unsigned long long int result = 1;
    while(exp) { result *= base; exp--; }
    return result;
}


/* -------------------------------------------- --------------  */


int main() 
{

  
  complex double c; // parameter of function fc(z) = z^2 + c
  complex double z_alfa; /*  alfa = parabolic fixed point z :  alfa = fc(alfa)   and  abs(d(alfa)) = 1.0  */ 
  complex double z_last; // last point of the ray ; it should be landing point = here alf afixed point 
  //
  unsigned long long int iPeriodParent = 1;
  //unsigned long long int iPeriodChild; // = ia_denominator 
  unsigned long long int ia_numerator = 13; // internal angle numerator
  unsigned long long int ia_denominator = 34; // internal angle denominator
  unsigned long long int ea_numerator = 2492769445; // external angle numerator
  unsigned long long int ea_denominator; // external angle denominator
  // 
  double distance;
  double PixelWidth = 0.003;  // = ("standard"" image width in world units )/ ("standard" image height in pixels ) = 3.0/1000

  // compute 
  //iPeriodChild = ia_denominator; 
  c= GiveC(ia_numerator, ia_denominator,1.0,iPeriodParent); // internal angle = , radius = 1.0, period = 1 
  z_alfa=GiveAlfaFixedPoint(c);
  ea_denominator = iPower(2, ia_denominator ) ; // ea_denominator = 2^ia_denominator - 1 
  printf("ea_denominator = %llu \n", ea_denominator);
  z_last = GiveDynamicRay(c, ea_numerator, ea_denominator,1, 100);
  distance = GiveDistance(z_alfa, z_last);

  // info 
  SaveInfoTextFile( iPeriodParent,
	            ia_numerator, ia_denominator, 
                    ea_numerator, ea_denominator,
    //                iPeriodChild, 
                    c, z_alfa, z_last,
                    PixelWidth, distance);

  return 0;
}
